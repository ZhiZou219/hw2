package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;

import static hangman.HangmanProperties.*;
import static settings.AppPropertyType.APP_WINDOW_HEIGHT;
import static settings.AppPropertyType.APP_WINDOW_WIDTH;

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee
 * @author Zhi Zou
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits



    Label             guiHeadingLabel;   // workspace (GUI) heading label
    HBox              headPane;          // conatainer to display the heading
    HBox              bodyPane;          // container for the main game displays
    ToolBar           footToolbar;       // toolbar for game buttons
    BorderPane        figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox              gameTextsPane;     // container to display the text-related parts of the game
    HBox              guessedLetters;    // text area displaying all the letters guessed so far
    HBox              remainingGuessBox; // container to display the number of remaining guesses
    Button            startGame;         // the button to start playing a game of Hangman
    HangmanController controller;
    VBox         allLetters;
    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (HangmanController) gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
    }

    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));

        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);

        figurePane = new BorderPane();
        figurePane.prefHeightProperty().setValue(Integer.parseInt(propertyManager.getPropertyValue(APP_WINDOW_HEIGHT)));
        figurePane.prefWidthProperty().setValue(Integer.parseInt(propertyManager.getPropertyValue(APP_WINDOW_WIDTH))/2);
        //add Hangman

        Line botL = new Line();
        botL.setFill(Color.valueOf("transparent"));
        botL.setStroke(Color.valueOf("transparent"));
        botL.setStartX(20);
        botL.setStartY(350);
        botL.setEndX(300);
        botL.setEndY(350);
        figurePane.getChildren().addAll(botL);

        Line leftL = new Line();
        leftL.setFill(Color.valueOf("transparent"));
        leftL.setStroke(Color.valueOf("transparent"));
        leftL.setStartX(20);
        leftL.setStartY(50);
        leftL.setEndX(20);
        leftL.setEndY(350);
        figurePane.getChildren().addAll(leftL);

        Line topL = new Line();
        topL.setFill(Color.valueOf("transparent"));
        topL.setStroke(Color.valueOf("transparent"));
        topL.setStartX(20);
        topL.setStartY(50);
        topL.setEndX(180);
        topL.setEndY(50);
        figurePane.getChildren().addAll(topL);

        Line lope = new Line();
        lope.setFill(Color.valueOf("transparent"));
        lope.setStroke(Color.valueOf("transparent"));
        lope.setStartX(180);
        lope.setStartY(50);
        lope.setEndX(180);
        lope.setEndY(80);
        figurePane.getChildren().addAll(lope);

        Circle head = new Circle(30);
        head.setStroke(Color.valueOf("transparent"));
        head.setFill(Color.valueOf("red"));
        head.setCenterX(180);
        head.setCenterY(110);
       figurePane.getChildren().addAll(head);

        Line body = new Line();
        body.setFill(Color.valueOf("transparent"));
        body.setStroke(Color.valueOf("transparent"));
        body.setStartX(180);
        body.setStartY(140);
        body.setEndX(180);
        body.setEndY(260);
        figurePane.getChildren().addAll(body);

        Line leg1 = new Line();
        leg1.setFill(Color.valueOf("transparent"));
        leg1.setStroke(Color.valueOf("transparent"));
        leg1.setStartX(180);
        leg1.setStartY(180);
        leg1.setEndX(130);
        leg1.setEndY(200);
        figurePane.getChildren().addAll(leg1);

        Line leg2 = new Line();
        leg2.setFill(Color.valueOf("transparent"));
        leg2.setStroke(Color.valueOf("transparent"));
        leg2.setStartX(180);
        leg2.setStartY(180);
        leg2.setEndX(230);
        leg2.setEndY(200);
        figurePane.getChildren().addAll(leg2);

        Line leg3 = new Line();
        leg3.setFill(Color.valueOf("transparent"));
        leg3.setStroke(Color.valueOf("transparent"));
        leg3.setStartX(180);
        leg3.setStartY(260);
        leg3.setEndX(150);
        leg3.setEndY(330);
        figurePane.getChildren().addAll(leg3);

        Line leg4 = new Line();
        leg4.setFill(Color.valueOf("transparent"));
        leg4.setStroke(Color.valueOf("transparent"));
        leg4.setStartX(180);
        leg4.setStartY(260);
        leg4.setEndX(210);
        leg4.setEndY(330);
        figurePane.getChildren().addAll(leg4);
        //add Hangman




        allLetters = new VBox();

        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();
        //boxPane = new StackPane();
        //boxPane.setStyle("-fx-background-color: black;");
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters,allLetters);

        bodyPane = new HBox();
        bodyPane.getChildren().addAll(figurePane, gameTextsPane);

        startGame = new Button("Start Playing");
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight);

        workspace = new VBox();
        workspace.getChildren().addAll(headPane, bodyPane, footToolbar);
    }

    private void setupHandlers() {
        startGame.setOnMouseClicked(e -> controller.start());
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }

    public Button getStartGame() {
        return startGame;
    }

    public BorderPane getFigurePane(){  return figurePane;}

    public void reinitialize() {
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();
        allLetters = new VBox();
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters,allLetters);
        bodyPane.getChildren().setAll(figurePane, gameTextsPane);
        for(int i =0; i < 10 ; i++){
            if(i==4){
                Circle head = (Circle)figurePane.getChildren().get(4);
                head.setFill(Color.valueOf("transparent"));
                head.setStroke(Color.valueOf("transparent"));
            }
            else{
                Line line = (Line)figurePane.getChildren().
                        get(i);
                line.setFill(Color.valueOf("transparent"));
                line.setStroke(Color.valueOf("transparent"));
            }
        }
    }
}
