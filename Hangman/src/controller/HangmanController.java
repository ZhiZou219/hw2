package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 * @author Zhi Zou
 */
public class HangmanController implements FileController {

    public enum GameState {
        UNINITIALIZED,
        INITIALIZED_UNMODIFIED,
        INITIALIZED_MODIFIED,
        ENDED
    }

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private GameState   gamestate;   // the state of the game being shown in the workspace
    private Text[]      progress;    // reference to the text area for the word
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private Path        workFile;
    private StackPane   boxPane ;
    private VBox    allLetters;
    private  HBox guessedLetters;
    private BorderPane figurePane;
    private boolean hint;
    private Button hintB = new Button("Hint!");


    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void disableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(true);
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }

    public GameState getGamestate() {
        return this.gamestate;
    }

    /**
     * In the homework code given to you, we had the line
     * gamedata = new GameData(appTemplate, true);
     * This meant that the 'gamedata' variable had access to the app, but the data component of the app was still
     * the empty game data! What we need is to change this so that our 'gamedata' refers to the data component of
     * the app, instead of being a new object of type GameData. There are several ways of doing this. One of which
     * is to write (and use) the GameData#init() method.
     */
    public void start() {
        gamedata = (GameData) appTemplate.getDataComponent();
        success = false;
        discovered = 0;
        hint = false;
        hintB.setDisable(false);
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

        gamedata.init();
        String check = gamedata.getTargetWord();
        int countUnique = 0;

        for(int i = 0; i < check.length() ; i++){
            if(!Character.isLetter(check.charAt(i))) {
                start();
            }
        }
        int charCount [] = new int[26];
        for(int i = 0 ; i < 26 ; i++){
            charCount[i]=0;
        }
        for(int i = 0 ; i<check.length() ; i++){
            int diff = check.charAt(i)-'a';
            charCount[diff]++;
        }
        for(int i = 0; i < 26 ; i++){
            if(charCount[i]>0){
                countUnique++;
            }
        }
        if(countUnique > 7){
            hint = true;
        }

        setGameState(GameState.INITIALIZED_UNMODIFIED);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        allLetters = (VBox) gameWorkspace.getGameTextsPane().getChildren().get(2);
        figurePane = gameWorkspace.getFigurePane();
        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        initWordGraphics(guessedLetters,allLetters);
        play();
    }

    private void end() {
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameButton.setDisable(true);
        setGameState(GameState.ENDED);
        hintB.setDisable(true);

        appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
        Platform.runLater(() -> {
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            String                    endMessage = manager.getPropertyValue(success ? GAME_WON_MESSAGE : GAME_LOST_MESSAGE);
            if (!success)
                endMessage += String.format(" (the word was \"%s\")", gamedata.getTargetWord());
            if (dialog.isShowing())
                dialog.toFront();
            else {
                dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), endMessage);
                for(int i = 0 ; i < gamedata.getTargetWord().length() ; i++){
                    progress[i].setVisible(true);

                    if(!gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0))){
                        StackPane box = (StackPane)guessedLetters.getChildren().get(i);
                        Rectangle r = (Rectangle)box.getChildren().get(0);
                        r.setFill(Color.valueOf("#DDBC95"));
                    }
                }

            }
        });
    }

    private void initWordGraphics(HBox guessedLetters, VBox allLetters) {
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(false);
            progress[i].setFont(Font.font(20));

        }
        for(int i = 0; i < progress.length;i++){
            boxPane = new StackPane();
            Rectangle box = new Rectangle();
            box.setHeight(30);
            box.setWidth(30);
            box.rotateProperty().setValue(45);
            box.setFill(Color.valueOf("#5BC8AC"));
            box.setStroke(Color.valueOf("#34675C"));
            boxPane.getChildren().add(box);
            boxPane.getChildren().add(progress[i]);
            guessedLetters.getChildren().addAll(boxPane);
        }
        if(hint){
            Rectangle empty = new Rectangle(50,50);
            empty.setStroke(Color.valueOf("transparent"));
            empty.setFill(Color.valueOf("transparent"));
            guessedLetters.getChildren().add(empty);

            hintB.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                    int random = (int) (Math.random()*gamedata.getTargetWord().length());
                    while(gamedata.getGoodGuesses().contains(gamedata.getTargetWord().charAt(random))){
                        random = (int) (Math.random()*gamedata.getTargetWord().length());
                    }
                    gamedata.addBadGuess('.');
                    addChoice(gamedata.getTargetWord().charAt(random));
                    hintB.setDisable(true);
                    gamedata.setUsedHint(true);
                    if(10-gamedata.getRemainingGuesses()==5){
                        Circle head = (Circle)figurePane.getChildren().get(4);
                        head.setFill(Color.valueOf("orange"));
                        head.setStroke(Color.valueOf("red"));
                    }
                    else{
                        Line line = (Line)figurePane.getChildren().
                                get(10-gamedata.getRemainingGuesses()-1);
                        line.setFill(Color.valueOf("red"));
                        line.setStroke(Color.valueOf("red"));
                    }

                }
            });
            guessedLetters.getChildren().add(hintB);
        }
        Text valuableOption = new Text("Remaining Options:");
        allLetters.getChildren().add(valuableOption);
        Text charList[] = new Text[26];
        String charC [] = {"a","b","c","d","e","f","g","h","i",
                "j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
        for(int i = 0; i < 26; i++){
            charList[i]= new Text(charC[i]);
            charList[i].setFont(Font.font(30));
        }
        HBox row1 = new HBox();
        for(int i =0 ; i<7;i++){
            boxPane = new StackPane();
            Rectangle box = new Rectangle();
            box.setHeight(50);
            box.setWidth(50);
            box.setFill(Color.valueOf("#E4EA8C"));
            box.setStroke(Color.valueOf("#50312F"));
            boxPane.getChildren().add(box);
            boxPane.getChildren().add(charList[i]);
            row1.getChildren().addAll(boxPane);
        }
        allLetters.getChildren().add(row1);
        HBox row2 = new HBox();
        for(int i =7 ; i<14;i++){
            boxPane = new StackPane();
            Rectangle box = new Rectangle();
            box.setHeight(50);
            box.setWidth(50);
            box.setFill(Color.valueOf("#E4EA8C"));
            box.setStroke(Color.valueOf("#50312F"));
            boxPane.getChildren().add(box);
            boxPane.getChildren().add(charList[i]);
            row2.getChildren().addAll(boxPane);
        }
        allLetters.getChildren().add(row2);

        HBox row3 = new HBox();
        for(int i =14 ; i<21;i++){
            boxPane = new StackPane();
            Rectangle box = new Rectangle();
            box.setHeight(50);
            box.setWidth(50);
            box.setFill(Color.valueOf("#E4EA8C"));
            box.setStroke(Color.valueOf("#50312F"));
            boxPane.getChildren().add(box);
            boxPane.getChildren().add(charList[i]);
            row3.getChildren().addAll(boxPane);
        }
        allLetters.getChildren().add(row3);

        HBox row4 = new HBox();
        for(int i =21 ; i<26;i++){
            boxPane = new StackPane();
            Rectangle box = new Rectangle();
            box.setHeight(50);
            box.setWidth(50);
            box.setFill(Color.valueOf("#E4EA8C"));
            box.setStroke(Color.valueOf("#50312F"));
            boxPane.getChildren().add(box);
            boxPane.getChildren().add(charList[i]);
            row4.getChildren().addAll(boxPane);
        }
        allLetters.getChildren().add(row4);



    }
    //help method clip and paste into it
    public void addChoice(char guess){
        int checkIsChar = guess - 'a';
        if (checkIsChar < 26 && checkIsChar >= 0) {
            if (!alreadyGuessed(guess)) {
                boolean goodguess = false;
                for (int i = 0; i < progress.length; i++) {
                    if (gamedata.getTargetWord().charAt(i) == guess) {
                        progress[i].setVisible(true);
                        gamedata.addGoodGuess(guess);
                        goodguess = true;
                        discovered++;
                    }
                }
                if (!goodguess) {
                    gamedata.addBadGuess(guess);

                    if(10-gamedata.getRemainingGuesses()==5){
                        Circle head = (Circle)figurePane.getChildren().get(4);
                        head.setFill(Color.valueOf("orange"));
                        head.setStroke(Color.valueOf("red"));
                    }
                    else{
                        Line line = (Line)figurePane.getChildren().
                                get(10-gamedata.getRemainingGuesses()-1);
                        line.setFill(Color.valueOf("red"));
                        line.setStroke(Color.valueOf("red"));
                    }
                }
                success = (discovered == progress.length);
                remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                //change color
                int charN = guess - 'a';
                if (charN < 7) {
                    int changeC = charN - 0;
                    HBox row = (HBox) allLetters.getChildren().get(1);
                    StackPane a = (StackPane) row.getChildren().get(changeC);
                    Rectangle b = (Rectangle) a.getChildren().get(0);
                    b.setFill(Color.valueOf("#CB6318"));
                } else if (charN < 14) {
                    int changeC = charN - 7;
                    HBox row = (HBox) allLetters.getChildren().get(2);
                    StackPane a = (StackPane) row.getChildren().get(changeC);
                    Rectangle b = (Rectangle) a.getChildren().get(0);
                    b.setFill(Color.valueOf("#CB6318"));
                } else if (charN < 21) {
                    int changeC = charN - 14;
                    HBox row = (HBox) allLetters.getChildren().get(3);
                    StackPane a = (StackPane) row.getChildren().get(changeC);
                    Rectangle b = (Rectangle) a.getChildren().get(0);
                    b.setFill(Color.valueOf("#CB6318"));
                } else if (charN < 26) {
                    int changeC = charN - 21;
                    HBox row = (HBox) allLetters.getChildren().get(4);
                    StackPane a = (StackPane) row.getChildren().get(changeC);
                    Rectangle b = (Rectangle) a.getChildren().get(0);
                    b.setFill(Color.valueOf("#CB6318"));
                }
            }
        }
        setGameState(GameState.INITIALIZED_MODIFIED);
    }

    public void play() {
        disableGameButton();

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    char guess = event.getCharacter().charAt(0);
                   addChoice(guess);

                });
                if (gamedata.getRemainingGuesses() <= 0 || success)
                    stop();

            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private void restoreGUI() {
        disableGameButton();
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize();

        guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        allLetters = (VBox) gameWorkspace.getGameTextsPane().getChildren().get(2);
        figurePane = (BorderPane) gameWorkspace.getFigurePane();

        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);

        success = false;
        restoreWordGraphics(guessedLetters,allLetters);
        play();
    }

    private void restoreWordGraphics(HBox guessedLetters, VBox allLetters) {
        discovered = 0;
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)));
            progress[i].setFont(Font.font(20));
            if (progress[i].isVisible())
                discovered++;
        }
        for(int i = 0; i < progress.length;i++){
            boxPane = new StackPane();
            Rectangle box = new Rectangle();
            box.setHeight(30);
            box.setWidth(30);
            box.rotateProperty().setValue(45);
            box.setFill(Color.valueOf("#5BC8AC"));
            box.setStroke(Color.valueOf("#34675C"));
            boxPane.getChildren().add(box);
            boxPane.getChildren().add(progress[i]);
            guessedLetters.getChildren().addAll(boxPane);
        }
        if(hint){
            Rectangle empty = new Rectangle(50,50);
            empty.setStroke(Color.valueOf("transparent"));
            empty.setFill(Color.valueOf("transparent"));
            guessedLetters.getChildren().add(empty);

            hintB.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                    int random = (int) (Math.random()*gamedata.getTargetWord().length());
                    while(gamedata.getGoodGuesses().contains(gamedata.getTargetWord().charAt(random))){
                        random = (int) (Math.random()*gamedata.getTargetWord().length());
                    }
                    gamedata.addBadGuess('.');
                    addChoice(gamedata.getTargetWord().charAt(random));

                    hintB.setDisable(true);
                    gamedata.setUsedHint(true);
                    if(10-gamedata.getRemainingGuesses()==5){
                        Circle head = (Circle)figurePane.getChildren().get(4);
                        head.setFill(Color.valueOf("orange"));
                        head.setStroke(Color.valueOf("red"));
                    }
                    else{
                        Line line = (Line)figurePane.getChildren().
                                get(10-gamedata.getRemainingGuesses()-1);
                        line.setFill(Color.valueOf("red"));
                        line.setStroke(Color.valueOf("red"));
                    }
                }
            });
            guessedLetters.getChildren().add(hintB);
        }
        //guessedLetters.getChildren().addAll(progress);
        Text valuableOption = new Text("Remaining Options:");
        allLetters.getChildren().add(valuableOption);
        Text charList[] = new Text[26];
        String charC [] = {"a","b","c","d","e","f","g","h","i",
                "j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
        for(int i = 0; i < 26; i++){
            charList[i]= new Text(charC[i]);
            charList[i].setFont(Font.font(30));
        }
        HBox row1 = new HBox();
        for(int i =0 ; i<7;i++){
            boxPane = new StackPane();
            Rectangle box = new Rectangle();
            box.setHeight(50);
            box.setWidth(50);
            box.setFill(Color.valueOf("#E4EA8C"));
            box.setStroke(Color.valueOf("#50312F"));
            boxPane.getChildren().add(box);
            boxPane.getChildren().add(charList[i]);
            row1.getChildren().addAll(boxPane);
        }
        allLetters.getChildren().add(row1);
        HBox row2 = new HBox();
        for(int i =7 ; i<14;i++){
            boxPane = new StackPane();
            Rectangle box = new Rectangle();
            box.setHeight(50);
            box.setWidth(50);
            box.setFill(Color.valueOf("#E4EA8C"));
            box.setStroke(Color.valueOf("#50312F"));
            boxPane.getChildren().add(box);
            boxPane.getChildren().add(charList[i]);
            row2.getChildren().addAll(boxPane);
        }
        allLetters.getChildren().add(row2);

        HBox row3 = new HBox();
        for(int i =14 ; i<21;i++){
            boxPane = new StackPane();
            Rectangle box = new Rectangle();
            box.setHeight(50);
            box.setWidth(50);
            box.setFill(Color.valueOf("#E4EA8C"));
            box.setStroke(Color.valueOf("#50312F"));
            boxPane.getChildren().add(box);
            boxPane.getChildren().add(charList[i]);
            row3.getChildren().addAll(boxPane);
        }
        allLetters.getChildren().add(row3);

        HBox row4 = new HBox();
        for(int i =21 ; i<26;i++){
            boxPane = new StackPane();
            Rectangle box = new Rectangle();
            box.setHeight(50);
            box.setWidth(50);
            box.setFill(Color.valueOf("#E4EA8C"));
            box.setStroke(Color.valueOf("#50312F"));
            boxPane.getChildren().add(box);
            boxPane.getChildren().add(charList[i]);
            row4.getChildren().addAll(boxPane);
        }
        allLetters.getChildren().add(row4);

        char guessed[]= {'a','b','c','d','e','f','g','h','i',
                'j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};

            for(int j = 0; j<26;j++){
                if(gamedata.getGoodGuesses().contains(guessed[j])){
                    int charN = guessed[j]-'a';
                        //change letter color
                    if (charN < 7) {
                        int changeC = charN - 0;
                        HBox row = (HBox) allLetters.getChildren().get(1);
                        StackPane a = (StackPane) row.getChildren().get(changeC);
                        Rectangle b = (Rectangle) a.getChildren().get(0);
                        b.setFill(Color.valueOf("#CB6318"));
                    } else if (charN < 14) {
                        int changeC = charN - 7;
                        HBox row = (HBox) allLetters.getChildren().get(2);
                        StackPane a = (StackPane) row.getChildren().get(changeC);
                        Rectangle b = (Rectangle) a.getChildren().get(0);
                        b.setFill(Color.valueOf("#CB6318"));
                    } else if (charN < 21) {
                        int changeC = charN - 14;
                        HBox row = (HBox) allLetters.getChildren().get(3);
                        StackPane a = (StackPane) row.getChildren().get(changeC);
                        Rectangle b = (Rectangle) a.getChildren().get(0);
                        b.setFill(Color.valueOf("#CB6318"));
                    } else if (charN < 26) {
                        int changeC = charN - 21;
                        HBox row = (HBox) allLetters.getChildren().get(4);
                        StackPane a = (StackPane) row.getChildren().get(changeC);
                        Rectangle b = (Rectangle) a.getChildren().get(0);
                        b.setFill(Color.valueOf("#CB6318"));
                    }
                    //change color over

                }
            }


        //bad
            for(int j = 0; j<26;j++){
                if(gamedata.getBadGuesses().contains(guessed[j])){
                    int charN = guessed[j]-'a';
                    //change letter color
                    if (charN < 7) {
                        int changeC = charN - 0;
                        HBox row = (HBox) allLetters.getChildren().get(1);
                        StackPane a = (StackPane) row.getChildren().get(changeC);
                        Rectangle b = (Rectangle) a.getChildren().get(0);
                        b.setFill(Color.valueOf("#CB6318"));
                    } else if (charN < 14) {
                        int changeC = charN - 7;
                        HBox row = (HBox) allLetters.getChildren().get(2);
                        StackPane a = (StackPane) row.getChildren().get(changeC);
                        Rectangle b = (Rectangle) a.getChildren().get(0);
                        b.setFill(Color.valueOf("#CB6318"));
                    } else if (charN < 21) {
                        int changeC = charN - 14;
                        HBox row = (HBox) allLetters.getChildren().get(3);
                        StackPane a = (StackPane) row.getChildren().get(changeC);
                        Rectangle b = (Rectangle) a.getChildren().get(0);
                        b.setFill(Color.valueOf("#CB6318"));
                    } else if (charN < 26) {
                        int changeC = charN - 21;
                        HBox row = (HBox) allLetters.getChildren().get(4);
                        StackPane a = (StackPane) row.getChildren().get(changeC);
                        Rectangle b = (Rectangle) a.getChildren().get(0);
                        b.setFill(Color.valueOf("#CB6318"));
                    }
                    //change color over

                }
            }

        for(int i =0; i < 10-gamedata.getRemainingGuesses() ; i++){
            if(i==4){
                Circle head = (Circle)figurePane.getChildren().get(4);
                head.setFill(Color.valueOf("orange"));
                head.setStroke(Color.valueOf("red"));
            }
            else{
                Line line = (Line)figurePane.getChildren().
                        get(i);
                line.setFill(Color.valueOf("red"));
                line.setStroke(Color.valueOf("red"));
            }
        }
    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize();
            enableGameButton();
        }
        if (gamestate.equals(GameState.ENDED)) {
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            FileChooser filechooser = new FileChooser();
            Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null)
                save(selectedFile.toPath());
        } else
            save(workFile);
    }

    @Override
    public void handleLoadRequest() throws IOException {
        boolean load = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            load = promptToSave();
        if (load) {
            PropertyManager propertyManager = PropertyManager.getManager();
            FileChooser     filechooser     = new FileChooser();
            Path            appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path            targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null && selectedFile.exists())
                load(selectedFile.toPath());
            hint = false;
            hintB.setDisable(false);
            int countUnique = 0;
            String check = gamedata.getTargetWord();
            int charCount [] = new int[26];
            for(int i = 0 ; i < 26 ; i++){
                charCount[i]=0;
            }
            for(int i = 0 ; i<check.length() ; i++){
                int diff = check.charAt(i)-'a';
                charCount[diff]++;
            }
            for(int i = 0; i < 26 ; i++){
                if(charCount[i]>0){
                    countUnique++;
                }
            }
            if(countUnique > 7){
                hint = true;
            }
            if(gamedata.isUsedHint()){
                hintB.setDisable(true);
            }
            restoreGUI(); // restores the GUI to reflect the state in which the loaded game was last saved
        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        int i;
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                               propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));

        setGameState(GameState.INITIALIZED_UNMODIFIED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gameworkspace.reinitialize();
        gamedata = (GameData) appTemplate.getDataComponent();
    }
}
